# Keycloak theme for SynchroniCity Projects
[![Email: lasse.lidegaard@alexandra.dk](https://img.shields.io/badge/contact-@LasseLidegaard-blue.svg?style=flat)](mailto:lasse.lidegaard@alexandra.dk) [![Language: CSS/HTML](https://img.shields.io/badge/lang-HTML/CSS-yellow.svg?style=flat)](http://www.keycloak.org/docs/3.3/server_development/topics/themes.html) [![License: AGPL-3.0](https://img.shields.io/badge/license-AGPL-lightgrey.svg?style=flat)](http://opensource.org/licenses/AGPL-3.0)

The SynchorniCity and OrganiCity projects both use keycloak as a major component for authentication. The theme originally made by OrganiCity has been re-created for the SynchroniCity project by [Lasse Lidegaard](mailto:lasse.lidegaard@alexandra.dk). This includes an update to support KeyCloak 3, which the OrganiCity theme does not. It is therefore only included for reference. It is KeyCloak 1.9 compatible.

## Building the Docker image
Building the image including the themes located in the src folder could not be easier.
```bash
docker build -t synchronicityiot/scenariotool-keycloak:3.4.3-1.0.0 \
             -t synchronicityiot/scenariotool-keycloak:latest .
```
The above example will tag the image with the version number of the embeded keycloal image version, and the theme version. Here the theme version is 1.0.0, and keycloak is 3.4.3. Also update the latest tag to point to the same image.

## Installing in production
The SynchroniCity project specifically has a server deployed at accounts.synchronicity-iot.eu, hosted by Aarhus University. The code below is an example, which may be replicated in other environments.
```bash
docker create   -e KEYCLOAK_USER=secret  \
                -e KEYCLOAK_PASSWORD=secret \
                -e PROXY_ADDRESS_FORWARDING=true \
                --restart always \
                --name keycloak \
                --net scenariotool \
                -p 8080:8080 -p 9990:8081 \
                -v /sincity/keycloak/data:/opt/jboss/keycloak/standalone/data \
                -v /sincity/keycloak/log:/opt/jboss/keycloak/standalone/log \
                   synchronicityiot/scenariotool-keycloak:3.4.3-1.0.0
```
It is obvious that the parameters must be tweaked for other environments, and this will of course look very different in a swarm environment.

# Thanks
A thank you shoud be sent out to [Dennis Boldt](https://www.itm.uni-luebeck.de/mitarbeitende/dennis-boldt.html) for helping to extract the OrganiCity theme from the OrganiCity project servers.